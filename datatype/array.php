<?php
/**
 * Created by PhpStorm.
 * User: android
 * Date: 5/15/2018
 * Time: 5:31 AM
 */

$names = array("Borzoo", "Nastaran", 20, "Yasaman");

for ($i = 0; $i < count($names); $i++) {
    echo $names[$i] . "<br>";
}
echo "---------" . "<br>";

foreach ($names as $name)
    echo $name . " " . "<br>";
echo "---------" . "<br>";

$students = array(
    "a" => array(
        "name" => "Nastaran",
        "family" => "Ghaffari",
        "age" => 30,
        "city" => "Tehran"
    ),
    "b" => array(
        "name" => "Borzoo",
        "family" => "Ghaderi",
        "age" => 31,
        "city" => "Tehran"
    )
);
echo $students['a']['family'] . "<br>";
echo "---------" . "<br>";

foreach ($students as $student)
    foreach ($student as $key => $value)
        echo $key . " " . $value . "<br>";
echo "---------" . "<br>";
?>
