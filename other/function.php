<?php
/**
 * Created by PhpStorm.
 * User: android
 * Date: 5/15/2018
 * Time: 6:42 AM
 * @param string $name
 */

function getDetail($name = "Ali")
{
    switch ($name) {
        case "Nastaran":
            echo "Nastaran is 30 years old!";
            break;
        case "Borzoo":
            echo "Borzoo is 31 years old!";
            break;
        default:
            echo "Ali is 10 years old!";
    }
}

getDetail('Nastaran');
echo "<br>";
getDetail();